import React, { PureComponent } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import Public from './Pages/Public.jsx';
import { AppContext } from './AppContext.jsx';
import EventEmitter from '../utils/EventEmitter.js';
import Login from './Pages/Login.jsx';
import PrivateRoute from './Templates/PrivateRoute.jsx';
import Private from './Pages/Private.jsx';

const client = new EventEmitter();

class Main extends PureComponent {
	state = {
		authenticated: false
	};

	componentDidMount() {
		client.on('LOGIN_SUCCESSFUL', this._loginSuccessListener);
	}

	componentWillUnmount() {
		client.removeEventListener('LOGIN_SUCCESSFUL', this._loginSuccessListener);
	}

	_loginSuccessListener = payload => {
		const { history } = this.props;
		this.setState({ authenticated: true });
		history.push('/private');
		payload.callback();
	};

	appContext = {
		client: client
	};

	render() {
		const { authenticated } = this.state;
		return (
			<AppContext.Provider value={ this.appContext }>
				<header>
					<Switch>
						<Route path='/login' render={ () => ( <p>Login has a different header!</p> ) }/>
						<Route path='/' render={ () => ( <p>Header</p> ) }/>
					</Switch>
				</header>
				<main>
					<Switch>
						<Route path='/login' component={ Login }/>
						<PrivateRoute
							authenticated={ authenticated }
							path='/private'
							component={ Private }/>
						<Route path='/' component={ Public }/>
					</Switch>
				</main>
			</AppContext.Provider>
		)
	}
}

export default withRouter(Main);