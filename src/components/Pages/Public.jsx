import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';

export default class Public extends PureComponent {
	render() {
		return (
			<>
				<h1>Public</h1>
				<Link to={'/login'}>Login page</Link>
			</>
		)
	}
}