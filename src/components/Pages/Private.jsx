import React, { PureComponent } from 'react';

export default class Private extends PureComponent {
	render() {
		console.log('I have rendered at least once!');
		return (
			<>
				<h1>Private! This will not be loaded unless logged in.</h1>
			</>
		)
	}
}