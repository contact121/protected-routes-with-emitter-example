import React, { PureComponent } from 'react';
import { AppContext } from '../AppContext.jsx';

export default class Login extends PureComponent {
	_onLogin = () => {
		const { client } = this.context;
		client.emit('LOGIN_SUCCESSFUL', { callback: this._loginSuccessfulCallback });
	};

	_loginSuccessfulCallback = () => {
		// setState, client.emit, anything really
		console.error('This function was emitted and then called in the receptor!!!');
	};

	render() {
		return (
			<>
				<h1>Login</h1>
				<button onClick={ this._onLogin }>Click me to login</button>
			</>
		)
	}
}

Login.contextType = AppContext;