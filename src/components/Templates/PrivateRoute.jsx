import React, { PureComponent } from 'react';
import { Route, Redirect } from 'react-router-dom';

export default class PrivateRoute extends PureComponent {
	_renderRoute = props => {
		const { component: Component, authenticated } = this.props;
		if (authenticated) {
			return (
				<Component { ...props }/>
			)
		}
		return (
			<Redirect to={ '/login' }/>
		)
	};

	render() {
		const { component: Component, authenticated, ...rest } = this.props;
		return (
			<Route { ...rest } render={ this._renderRoute }/>
		)
	}
}