import EE from 'eventemitter3';

export default class EventEmitter {
	constructor() {
		this.emitter = new EE();
	}

	on = (event, listener) => {
		this.emitter.on(event, listener);
	};

	removeEventListener = (event, listener) => {
		this.emitter.removeListener(event, listener);
	};

	emit = (event, payload, error = false) => {
		this.emitter.emit(event, payload, error);
	};
}